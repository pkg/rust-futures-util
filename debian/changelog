rust-futures-util (0.3.21-1+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 14:29:19 +0100

rust-futures-util (0.3.21-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 17:52:03 +0530

rust-futures-util (0.3.21-1) unstable; urgency=medium

  * Team upload.
  * Package futures-util 0.3.21 from crates.io using debcargo 2.5.0
  * Update relax-dep.patch for new upstream and current situation in Debian.
  * Add dev-dependency on futures so the benches work.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 05 Jul 2022 20:53:47 +0000

rust-futures-util (0.3.17-3) unstable; urgency=medium

  * Team upload.
  * Package futures-util 0.3.17 from crates.io using debcargo 2.4.4
  * Remove optional (in the rust sense) dependencies on old version of tokio to
    make the package instalable. (the "io-compat" feature that used them was
    already removed).

 -- Peter Michael Green <plugwash@debian.org>  Fri, 22 Oct 2021 09:54:14 +0000

rust-futures-util (0.3.17-2) unstable; urgency=medium

  * Team upload.
  * Package futures-util 0.3.17 from crates.io using debcargo 2.4.4
  * Source only upload for testing migration.

  [ Fabian Grünbichler ]
  * mark tests as broken, they require files from the futures workspace

 -- Peter Michael Green <plugwash@debian.org>  Thu, 21 Oct 2021 09:58:11 +0000

rust-futures-util (0.3.17-1) unstable; urgency=medium

  * Team upload.
  * Package futures-util 0.3.17 from crates.io using debcargo 2.4.4

  [ Andrej Shadura ]
  * Package futures-util 0.3.8 from crates.io using debcargo 2.4.3

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Fri, 15 Oct 2021 14:15:42 +0200
